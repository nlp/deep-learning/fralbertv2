import os
import glob
import argparse
import tensorflow as tf

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help="generic input without nbr.out", type=str)
parser.add_argument("-o", "--output", help="final output", type=str)
#parser.add_argument("-pc", "--percentage", help="percentage of the text we want to keep", type=str)

args = parser.parse_args()
input_generic = args.input
save_path = args.output


tfrecords_path = glob.glob(input_generic + '.*.trf_tmp')
print(tfrecords_path)
dataset = tf.data.TFRecordDataset(tfrecords_path)
writer = tf.data.experimental.TFRecordWriter(save_path)
writer.write(dataset)

