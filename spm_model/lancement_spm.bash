

models="spiece"
datas="wiki.fr.0.1.txt"


srun_variables="\
	-A wsn@cpu \
	--job-name=sp_${models}_train_${datas} \
	--cpus-per-task=8 \
	--time=05:00:00 "

echo "running with: $srun_variables"

date >& sp_train.log
time spm_train --model_prefix=$models --vocab_size=32000 --character_coverage=0.9999999 --num_threads=32 --input=$datas --train_extremely_large_corpus=true --shuffle_input_sentence=true --max_sentence_length=1024 --pad_piece="<pad>" --bos_piece="[CLS]" --eos_piece="[SEP]"  --user_defined_symbols="[CLS],[MASK]" --pad_id=0 --bos_id=2 --eos_id=3 --unk_id=1 >& sp_train.log  &



