#!/bin/bash
#SBATCH -A wsn@a100
#SBATCH --job-name=jb_bert_test
#SBATCH -C a100
#SBATCH --gpus-per-node=1
#SBATCH --time=05:00:00             # temps maximum d'execution demande (HH:MM:SS)




source ~/.bashrc
conda activate py309

pushd $WORK/Projects/frALBERTv2


models="spm_model/spiece"
datas="data/wiki.fr.0.2.txt"


srun_variables="\
-A wsn@cpu \
--job-name=sp_${models}_train_${datas} \
--cpus-per-task=8 \
--time=05:00:00 "

echo "running with: $srun_variables"

date >& sp_train.log
srun $srun_variables spm_train --model_prefix=$models --vocab_size=32000 --character_coverage=1.0 --num_threads=16 --input=$datas --train_extremely_large_corpus=true --shuffle_input_sentence=true --max_sentence_length=1024 >& sp_train.log  & 

popd

