#!/bin/bash
#SBATCH -A wsn@a100
#SBATCH -C a100
#SBATCH --gpus-per-node=1
#SBATCH --ntasks=1 --cpus-per-task=100
#SBATCH --job-name=frAv2_pretrain
#SBATCH --time=20:00:00             # temps maximum d'execution demande (HH:MM:SS)
#SBATCH --output=frAv2pt_%j_%x.out     # nom du fichier de sortie
#SBATCH --error=frAv2pt_%j_%x.err      # nom du fichier d'erreur (ici commun avec la sortie)



source ~/.conda_activate

conda activate py309
mdate=`date '+%Y-%m-%d_%H-%M-%S'`
pushd $WORK/Projects/frALBERTv2

rsync -a --progress frALBERTv2_model/* frALBERTv2_model.bck.$mdate/

list_input=`/bin/ls data/*.tfr | perl -pe 's/\s+/,/g' | perl -pe 's/,$//g'`

#list_input="data/liste_ville_pays_punct.tfr,data/wiki.fr.1.0.filtered.txt.249.tfr"
echo "Liste_fichiers apprentissage:"
echo $list_input

#sleep 60

srun python -m albert.run_pretraining \
    --input_file=$list_input \
    --output_dir=frALBERTv2_model \
    --do_train \
    --do_eval \
    --train_batch_size=512 \
    --eval_batch_size=64 \
    --max_seq_length=128 \
    --max_predictions_per_seq=20 \
    --optimizer='lamb' \
    --learning_rate=.00176 \
    --num_train_steps=2000000 \
    --num_warmup_steps=3125 \
    --albert_config_file=config_model_frALBERTv2/config.json \
    --save_checkpoints_steps=5000
popd
