#!/bin/bash
#SBATCH -A wsn@cpu
#SBATCH --job-name=frALBERTv2_cd
#SBATCH --ntasks=2 --cpus-per-task=5
#SBATCH --time=00:30:00             # temps maximum d'execution demande (HH:MM:SS)
#SBATCH --output=frAcd_stdout_%j_%x.out     # nom du fichier de sortie
#SBATCH --error=frAcd_stderr_%j_%x.err      # nom du fichier d'erreur (ici commun avec la sortie)



source ~/.conda_activate

conda activate py309

pushd $WORK/Projects/frALBERTv2


models="spm_model/spiece.fr.model"
vocab="spm_model/spiece.fr.vocab"
datas="data/wiki.fr.0.2.1.txt"

idfile=$1
dupenumber=$2

if [ ! -e "data/wiki.fr.1.0.filtered.txt.$idfile.$dupenumber.out" ]
then
srun python create_pretraining_data_multithreaded.py --spm_model_file spm_model/spiece.fr.model --input_file data/wiki.fr.1.0.filtered.txt.$idfile --max_seq_length 128 --output_file data/wiki.fr.1.0.filtered.txt.$idfile.$dupenumber.out --do_lower_case no --vocab_file spm_model/spiece.fr.vocab --threads 40 --dupe_factor 1
#echo $command_line
else
	echo "data/wiki.fr.1.0.filtered.txt.$idfile.$dupenumber.out processed"
fi
popd
