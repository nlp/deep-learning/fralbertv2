#*-* utf-8 *-*
import sys
import argparse 

parser = argparse.ArgumentParser()
parser.add_argument("-o", "--output", help="generic output (mind the dot)", type=str)
parser.add_argument("-i", "--input", help="input", type=str)
parser.add_argument("-n", "--ndoc", help="max number of documents", type=int)
args = parser.parse_args()

def print_int(i):
    istr = str(i)
    flength = 4-len(istr)
    fstr = ""
    for k in range(flength):
        fstr = fstr + "0"
    fstr = fstr + istr
    return istr


nbdoc = 0
incfile = 0
ofile = open(args.output+print_int(incfile),"w")
with open(args.input,"r") as ifile:
    for line in ifile:
        line = line.strip()
        ofile.write(line+"\n")
        if len(line) == 0 :
            nbdoc = nbdoc + 1
            if nbdoc % args.ndoc == 0 :
                incfile = incfile + 1
                ofile.close()
                ofile = open(args.output+print_int(incfile),"w")
    ofile.close()

