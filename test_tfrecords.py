import os
import glob
import argparse
import sys
import tensorflow as tf

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help="input", type=str)
parser.add_argument("-o", "--output", help="final output", type=str)
#parser.add_argument("-pc", "--percentage", help="percentage of the text we want to keep", type=str)

args = parser.parse_args()
input_generic = args.input
save_path = args.output


#tfrecords_path = glob.glob(input_generic)
print(input_generic)

tfrecords_path = glob.glob(input_generic)
dataset = tf.data.TFRecordDataset(tfrecords_path)
#data = tf.WholeFileReader.read(dataset)
try :
    writer = tf.data.experimental.TFRecordWriter(save_path)
    writer.write(dataset)
except:
    e = sys.exc_info()[0]
    print(e)

